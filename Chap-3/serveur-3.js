// ################################################## //
// ## Déterminer la page appelée et les paramètres ## //
// ################################################## //
var http = require('http');
var url = require('url');

var server = http.createServer(function (req, res) {
    var page = url.parse(req.url).pathname;
    console.log(page);
    res.writeHead(200, { "Content-Type": "text/plain" });

    if (page == '/') {
        res.write('Bienvenue à l\'accueil de notre hôtel, que pus-je pour vous ?');
    } else if (page == '/sous-sol') {
        res.write('Que faites vous dans la cave à vin !!!');
    } else if (page == '/etage/1/chambre') {
        res.write('Hé ho, C\'est privé ici !!!');
    } else if (page == '/joel') {
        res.write('Cette toute represente mon prenom!!!');
    }
    res.end();
});
server.listen(8080);