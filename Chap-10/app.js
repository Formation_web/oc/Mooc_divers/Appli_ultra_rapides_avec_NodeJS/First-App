var http = require('http');
var fs = require('fs');

// Chargement du fichier index.html affiché au client
var server = http.createServer(function (req, res) {
    fs.readFile('./index.html', 'utf-8', function (error, content) {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end(content);
    });
});

// Chargement de socket.io
var io = require('socket.io').listen(server);

// Ajout d'un ecouteur sur l'évènement Connection
io.sockets.on('connection', function (socket, pseudo) {
    // Quand un client se connecte onlui envoie un message
	socket.emit('message', 'Vous êtes bien connecté !');
	// On signale aux autres clients qu'il y a un nouveau venu
	socket.broadcast.emit('message', 'Un autre client vient de se connecter !');

	// Dès que l'on nous donne un pseudo, on le stocke avec une variable de session
    socket.on('petit_nouveau', function(pseudo) {
        socket.pseudo = pseudo;
    });

    // Dès que l'on reçoit un "message" (clic sur le bouton), on le note dans la console
	socket.on('message', function (message) {
	    // On récupère le pseudo de celui qui a cliqué dans la variable de session
		console.log(socket.pseudo + ' me parle ! Il me dit : ' + message);
	});
});

// On écoute le port local :8080
server.listen(8080);