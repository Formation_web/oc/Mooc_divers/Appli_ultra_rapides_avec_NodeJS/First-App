// ################################################## //
// ## Emettre des évènnements ## //
// ################################################## //


var EventEmitter = require('events').EventEmitter;
var jeu = new EventEmitter();

var test = new EventEmitter();

jeu.on('gameover', function(message) {
    console.log(message);
});

test.on('gagner', function(message) {
    console.log(message);
});

test.emit("gagner", 'Vous avez gagné !');

jeu.emit('gameover', 'vous avez perdu !');
